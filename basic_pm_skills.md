- Disregarding your own biases and listening to your team and customers  
- [x] Grew asset tracking for customers from 1% open rate to 23% by switching from email to sms.  

- Tenacity, not giving up when faced with failure  
- [x] The tracking product for customers went through 12 iterations over 2 months before reaching peak performance. I was being told "the market isn't ready", but I knew it was all about figuring out the right UX and I had to push my vision to get the work done.  

- Getting things done start to finish  
- [x] Implemented a completely digital logistics system in a country where our competitors were carrying around stacks of paper to document their work.  
- Achieve results when the deliverables are unclear  
- [x] Set up technology that was pioneering in its market (mobile based tracking, sms confirmations, courier apps with navigation). We developed these solutions by looking at the needs of the market, and the technological possibilities.
- Lead through influence, not authority  
- [x] Recruiting staff from a risk averse culture to work for a startup  
- Exposure to the current working paradigms in technology  
- [x] Visited Silicon Valley and learned from some of the best about [rapid prototyping](http://www.tomchi.com/) and [design thinking](https://classics.stanford.edu/people/michael-shanks).